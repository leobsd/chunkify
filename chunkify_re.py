"""
Utility to split strings up into chunks, filling remaining characters
"""

import re

def chunkify_re(string, chunk_length, fill="x"):
    """
    Groups characters from an input string into fixed length chunks
    with a given fill value

    :param string: String to be split in chunks
    :type string: str
    :param chunk_length: Length of each chunked substring
    :type chunk_length: int
    :param fill: String fill any remaining space at the end of <chunks>
    :type fill: str
    :returns: A list of chunked substrings of length <chunk_length>
    :rtype: list

    Example:
    chunkify(‘ABCDEFG’, 3, ‘x’) -> [‘ABC’, ‘DEF’, Gxx’]
    """
    if not isinstance(string, str):
        raise TypeError("expected a string")

    if not isinstance(chunk_length, int):
        raise TypeError("expected an integer")

    if not isinstance(fill, str):
        raise TypeError("expected a string")

    if len(fill) == 0:
        raise ValueError("expected non-empty fill string")

    if not string or string.isspace():
        return []

    string_length = len(string)  # avoid running an O(n) operation twice
    chunks = re.findall(f'.{1,{chunk_length}}', string)

    if string_length % chunk_length != 0:
        # need to fill remaining characters
        fill_length = len(fill)
        # concatenate as many full copies of fill as possible
        chunks[-1] += fill * ((chunk_length - len(chunks[-1])) // fill_length)
        # fill the rest with the longest subset of fill
        chunks[-1] += fill[0 : chunk_length - len(chunks[-1])]

    return chunks
