import unittest

from chunkify import chunkify


class ChunkTests(unittest.TestCase):
    def test_string_invalid(self):
        with self.assertRaises(TypeError):
            chunkify(None, 1)

    def test_string_empty(self):
        assert chunkify("", 1) == []
        assert chunkify("    ", 1) == []

    def test_string_len_exact(self):
        assert chunkify("A", 1) == ["A"]
        assert chunkify("AB", 1) == ["A", "B"]

    def test_string_fill(self):
        # simple filling
        assert chunkify("ABCDEFG", 3, "x") == ["ABC", "DEF", "Gxx"]

        # must also work preserve empty spaces
        assert chunkify("A C E G", 3, "x") == ["A C", " E ", "Gxx"]

        # no restriction on fill length: exact division -> N complete fills
        assert chunkify("ABCDEFG", 3, "xy") == ["ABC", "DEF", "Gxy"]

        # no restriction on fill length: non-exact division -> partial trailing fill
        assert chunkify("ABCDEFG", 5, "xy") == ["ABCDE", "FGxyx"]
        assert chunkify("ABCDEFG", 8, "xyz") == ["ABCDEFGx"]

    def test_chunk_length_invalid(self):
        with self.assertRaises(TypeError):
            chunkify("string", None)

    def test_fill_invalid(self):
        with self.assertRaises(TypeError):
            assert chunkify("string", 1, None)

        with self.assertRaises(ValueError):
            assert chunkify("string", 1, "")
