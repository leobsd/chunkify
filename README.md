# Chunkify

Function to split up a given string into equal-size chunks, filling the remainder of the last chunk with a predefined filling character.

Discarded solutions:
* **textwrap**: doesn't work with spaces (e.g.: `textwrap.wrap("AB CD EF", 2)` results in `['AB', 'CD', 'EF']`
* **re**: replacing the list comprehension with `re.findall(f'.{1,{chunk_length}}', string)` is much easier to read, but performs much worse (10 to 25x) as the string and chunk lengths grow. More details on the performance section below

## coverage
```
pytest -vv --cov=myproc chunkify_test.py
```

## Formatting
```
python3 -m black chunkify.py
```

## Linting
```
python3 -m pylint chunkify.py
```

## Performance of list-comprehension vs. regex

Compare execution time of list-comprehension-based chunkify vs. regex-based, with multiple string lengths and chunk sizes. Output as a CSV sheet. times in miliseconds

```
./chunkify_perf.sh
```
Preview of the results [here](https://docs.google.com/spreadsheets/d/11WDYkxs9lxQLMLgzw_VAEsahR_Q2cm2Ru80cXUV39hk/edit?usp=sharing)
