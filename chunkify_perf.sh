#!/bin/bash -e
# Compare execution time of list-comprehension-based chunkify vs. regex-based,
# with multiple string lengths and chunk sizes. Output as a CSV sheet; times in
# miliseconds

len=( 10 100 1000 10000 100000 1000000 10000000 100000000 )

( IFS=,; echo "str_len,${len[*]}" )

for str_len in "${len[@]}"; do
    for script in chunkify chunkify_re; do
        # First column is the method name and string length
        results=( ${script}_${str_len} )

        for chunk_len in "${len[@]}"; do
            [ ${chunk_len} -gt ${str_len} ] && break # diregard impact of fill

            runnable="from ${script} import ${script}; ${script}(${str_len}*'X', ${chunk_len})"
            read -r _ _ _ _ _ time unit _ _ < <(python3 -m timeit "${runnable}")

            # Base time unit is miliseconds; convert if needed
            case ${unit} in
                usec) time="$(echo "scale=5; ${time} / 1000" | bc -l)";;
                sec) time="$(echo "scale=5; ${time} * 1000" | bc -l)";;
            esac

            results+=( ${time} )
        done

        # Output in CSV
        ( IFS=,; echo "${results[*]}" )
    done
done
